# tetrayder-ultra README

Tetrayder-ultra is a programming script language specialized on optimization different parametrs.

## Features

This excention helps users to write code on script language. All you need is to enable an extention and it will works with .tdr files.

![alt-text2](https://gitlab.com/i.baranov/tetrayder-ultra-vscode-extention/-/raw/master/images/exampleWithourtExtention.png)
![alt-text2](https://gitlab.com/i.baranov/tetrayder-ultra-vscode-extention/-/raw/master/images/exapleWithExtention.png)

## Requirements

No dependencies are needed.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of Tetrayder ULTRA